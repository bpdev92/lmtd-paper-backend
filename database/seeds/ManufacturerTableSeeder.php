<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Manufacturer;
use Ramsey\Uuid\Uuid;

class ManufacturerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      $manufacturer = new Manufacturer();
      $manufacturer->id = Uuid::uuid4();
      $manufacturer->name = "Unknown";
      $manufacturer->save();

      $manufacturer = new Manufacturer();
      $manufacturer->id = 'new';
      $manufacturer->name = "- New Manufacturer -";
      $manufacturer->save();
    }
}