<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Vendor;
use Ramsey\Uuid\Uuid;

class VendorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


      $vendors = [
        [
          'name' => 'Artist',
          'established' => null,
          'location' => null
        ],
        [
          'name' => 'Mondo',
          'established' => '2004',
          'location' => 'Austin, TX'
        ],
        [
          'name' => 'Grey Matter Art',
          'established' => '2013',
          'location' => 'New York, NY'
        ],
        [
          'name' => 'Bottleneck Gallery',
          'established' => '2012',
          'location' => 'New York, NY'
        ],
        [
          'name' => 'Black Dragon Press',
          'established' => '2014',
          'location' => 'London, UK'
        ],
        [
          'name' => 'Acme Archives',
          'established' => '2009',
          'location' => 'Burbank, CA'
        ],
        [
          'name' => 'Fifty-Nine Parks',
          'established' => '2016',
          'location' => 'Austin, TX'
        ],
        [
          'name' => 'Cyclops Print Works',
          'established' => '2000',
          'location' => 'Canoga Park, CA'
        ]
      ];

      foreach( $vendors as $vendor ) {
        $saveVendor = new Vendor();
        
        $saveVendor->id = Uuid::uuid4();
        $saveVendor->name = $vendor['name'];
        $saveVendor->established = $vendor['established'];
        $saveVendor->location = $vendor['location'];
        $saveVendor->save();
      }

      $saveVendor = new Vendor();
        
        $saveVendor->id = 'new';
        $saveVendor->name = '- New Vendor -';
        $saveVendor->established = null;
        $saveVendor->location = null;
        $saveVendor->save();


    }
}