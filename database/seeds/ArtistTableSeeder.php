<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Artist;
use Ramsey\Uuid\Uuid;

class ArtistTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $artist = new Artist();
      $artist->id = "new";
      $artist->name = "- New Artist -";
      $artist->save();
    }
}