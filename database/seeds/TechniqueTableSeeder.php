<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Technique;
use Ramsey\Uuid\Uuid;

class TechniqueTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


      $techniques = [
        'Unknown',
        'Aquatint',
        'Collagraph',
        'Digital',
        'Engraving',
        'Etching',
        'Giclee',
        'Lenticular',
        'Letterpress',
        'Linocut',
        'Lithograph',
        'Mezzotint',
        'Offset Lithograph',
        'Screen Print',
        'Wood Engraving',
        'Woodcut',
      ];

      foreach( $techniques as $technique ) {
        $saveTechnique = new Technique();
        
        $saveTechnique->id = Uuid::uuid4();
        $saveTechnique->name = $technique;
        $saveTechnique->save();
      }

      $technique = new Technique();
      $technique->id = "new";
      $technique->name = "- New Technique -";
      $technique->save();

    }
}