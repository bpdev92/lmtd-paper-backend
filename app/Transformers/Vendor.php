<?php
namespace App\Transformers;

use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class Vendor extends TransformerAbstract
{
    public function transform(\App\Models\Vendor $vendor)
    {
        return [
            'id'           => $vendor->id,
            'name'         => $vendor->name,
            'established'  => $vendor->established,
            'location'     => $vendor->location,
        ];
    }
}
