<?php
namespace App\Transformers\AddPrint;

use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class Artist extends TransformerAbstract
{
    public function transform(\App\Models\Artist $artist)
    {
        return [
            'value'       => $artist->id,
            'text'        => $artist->name,
        ];
    }
}
