<?php
namespace App\Transformers\AddPrint;

use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class Manufacturer extends TransformerAbstract
{
  public function transform(\App\Models\Manufacturer $manufacturer)
  {
    return [
      'value'       => $manufacturer->id,
      'text'        => $manufacturer->name,
    ];
  }
}
