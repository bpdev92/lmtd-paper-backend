<?php
namespace App\Transformers\AddPrint;

use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class Technique extends TransformerAbstract
{
    public function transform(\App\Models\Technique $technique)
    {
        return [
            'value'       => $technique->id,
            'text'        => $technique->name,
        ];
    }
}
