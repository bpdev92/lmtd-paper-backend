<?php
namespace App\Transformers\AddPrint;

use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class Vendor extends TransformerAbstract
{
    public function transform(\App\Models\Vendor $vendor)
    {
        return [
            'value'       => $vendor->id,
            'text'        => $vendor->name,
        ];
    }
}
