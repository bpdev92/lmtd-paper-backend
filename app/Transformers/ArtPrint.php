<?php
namespace App\Transformers;

use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class ArtPrint extends TransformerAbstract
{
    public function transform(\App\Models\ArtPrint $print)
    {
        return [
            'id' => $print->id,
            'title' => $print->title,
            'artist' => $print->artist,
            'edition' => $print->edition,
            'edition_size' => $print->edition_size,
            'technique' => $print->technique,
            'manufacturer' => $print->manufacturer,
            'vendor' => $print->vendor,
            'picture' => $print->picture,
            'original_price' => $print->original_price,
            'release_date' => $print->release_date,
            'width' => $print->width,
            'height' => $print->height,
            'status' => $print->status,
            'edition' => $print->edition,
            'created_at' => $print->created_at,
            'updated_at' => $print->updated_at
        ];
    }
}


