<?php
namespace App\Transformers;

use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class Artist extends TransformerAbstract
{
    public function transform(\App\Models\Artist $artist)
    {
        return [
            'id'          => $artist->id,
            'name'        => $artist->name,
        ];
    }
}
