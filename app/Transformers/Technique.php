<?php
namespace App\Transformers;

use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class Technique extends TransformerAbstract
{
    public function transform(\App\Models\Technique $technique)
    {
        return [
            'id'          => $technique->id,
            'name'        => $technique->name,
        ];
    }
}
