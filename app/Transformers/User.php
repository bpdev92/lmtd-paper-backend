<?php
namespace App\Transformers;

use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class User extends TransformerAbstract
{
    public function transform(\App\Models\User $user)
    {
        return [
            'id'           => $user->id,
            'firstName'   => $user->first_name,
            'lastName'    => $user->last_name,
            'username'     => $user->username,
            'email'        => $user->email
        ];
    }
}
