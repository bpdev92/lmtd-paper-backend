<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Ramsey\Uuid\Uuid;
use App\Models\ArtPrint;
use App\Models\Artist;
use App\Models\Manufacturer;
use App\Models\Technique;
use App\Models\Vendor
;
class MondoCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "scrape:mondo";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Import Mondo prints from csv";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      $mondo = Vendor::where('name', 'Mondo')->first();
      $unknownManufacturer = Manufacturer::where('name', 'Unknown')->first();
      $unknownTechnique = Technique::where('name', 'Unknown')->first();
      
      $handle = fopen(storage_path("scrape-csvs/mondo.csv"), "r");
      $headers = true;
      while (($print = fgetcsv($handle, 1000, ",")) !== FALSE) 
      {
        if($headers) { $headers = false; continue; }

        preg_match('/(?<=\().+?(?=\))/', $print[0], $edition);
        
        if($edition) {
          $edition = $edition[0];
          $print[0] = preg_replace("/\(([^()]*+|(?R))*\)/","", $print[0]);
        }else{
         $edition = "Regular";
        }

        $newPrint = new ArtPrint();
        $newPrint->id = Uuid::uuid4();
        $newPrint->title = $print[0];
        $newPrint->artist_id = $this->artist($print[1]);
        $newPrint->picture = $print[3];
        $newPrint->edition_size = $print[4];
        $newPrint->technique_id = $unknownTechnique->id;
        $newPrint->manufacturer_id = $print[6] === "" || $print[6] === "null" ? $unknownManufacturer->id : $this->manufacturer($print[6]);
        $newPrint->vendor_id = $mondo->id;
        $newPrint->original_price = $print[7];
        $newPrint->release_date = null;
        $newPrint->width = $print[9];
        $newPrint->height = $print[10];
        $newPrint->status = $print[11];
        $newPrint->edition = $edition;

        $newPrint->save();
      }



      fclose($handle);
    }


    public function artist($artistName) {

      $artist = Artist::where('name', $artistName)->first();

      if($artist){
        $artistId = $artist->id;
      }else{
        $newArtist = new Artist();
        $newArtist->id = Uuid::uuid4();
        $newArtist->name = $artistName;
        $newArtist->save();
        $artistId = $newArtist->id;
      }

      return $artistId;

    }


    public function manufacturer($manufacturerName) {

      $manufacturer = Manufacturer::where('name', $manufacturerName)->first();

      if($manufacturer){
        $manufacturerId = $manufacturer->id;
      }else{
        $newManufacturer = new Manufacturer();
        $newManufacturer->id = Uuid::uuid4();
        $newManufacturer->name = $manufacturerName;
        $newManufacturer->save();
        $manufacturerId = $newManufacturer->id;
      }

      return $manufacturerId;

    }


}
