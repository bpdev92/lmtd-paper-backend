<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArtPrint extends Model
{
    protected $table = 'prints';

    public $incrementing = false;


    public function artist() {
        return $this->belongsTo('App\Models\Artist');
    }

    public function vendor() {
        return $this->belongsTo('App\Models\Vendor');
    }

    public function manufacturer() {
        return $this->belongsTo('App\Models\Manufacturer');
    }

    public function technique() {
        return $this->belongsTo('App\Models\Technique');
    }



}
