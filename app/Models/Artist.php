<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
    protected $table = 'artists';

    public $incrementing = false;


    public function ArtPrints() {
        return $this->hasMany('App\Models\ArtPrint');
    }

}
