<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidEditionSize implements Rule
{
  public function __construct()
  {

  }

  public function passes($attribute, $value) 
  {
    $value = trim(strtolower($value));

    if($value === 'timed edition' || is_numeric($value)){
     return true;
    }

    return false;
    
  }

  public function message()
  {
    return "Edition Size must be a whole number or must say 'Timed Edition'";
  }


}