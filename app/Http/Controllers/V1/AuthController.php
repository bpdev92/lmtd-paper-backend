<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\User;
use Ramsey\Uuid\Uuid;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    protected $issuerOfToken = 'lmtd-paper';
    protected $reponse = '';

    public function __construct()
    {
        //
    }


    protected function jwt(User $user) {
        $payload = [
            'iss' => $this->issuerOfToken, // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued. 
            'exp' => time() + 60 * 60 * 24 // Expiration time is a month from the time issued
        ];
        
        return JWT::encode($payload, env('JWT_SECRET'));
    } 


    public function authenticate(Request $request)
    {
        $this->validate($request, [
          'email'      => 'required|email',
          'password'   => 'required'
        ]);

        $user = User::where('email', $request->get('email'))->first();
    
        if (!$user) {

            $this->response = response()->json([
                'data' => [
                    'errors' => 'Email or password is incorrect.'
                ]
            ], 401);
        }else{

            if (!Hash::check($request->get('password'), $user->password)) {
                $this->response = response()->json([
                    'data' => [
                        'errors' => 'Email or password is incorrect.'
                    ]
                ], 401);
            }else{

                $usr = [
                    'id' => $user->id,
                    'email' => $user->email,
                    'username' => $user->username,
                ];

                $this->response = response()->json([
                    'data' => [
                        'user'  => $usr,
                        'token' => $this->jwt($user)
                    ]
                ], 200);
            }
        }
        return $this->response;
    }


    public function register(Request $request){

        $this->validate($request, [
            'email'      => 'required|email|unique:users',
            'username'   => 'required|alpha_num|unique:users',
            'password'   => 'required|confirmed'
          ]);
  
        $user             = new User();
        $user->id         = Uuid::uuid1();
        $user->email      = $request->get('email');
        $user->username   = $request->get('username');
        $user->password   = app('hash')->make($request->get('password'));
        $user->save();


        $usr = [
            'id' => $user->id,
            'email' => $user->email,
            'username' => $user->username,
        ];


        $this->response = response()->json([
            'data' => [
                'user'  => $usr,
                'token' => $this->jwt($user)
            ]
        ], 200);


        return $this->response;
    }


    
}
