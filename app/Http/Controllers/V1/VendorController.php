<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Manager;
use App\Models\Vendor;
use Ramsey\Uuid\Uuid;

class VendorsController extends Controller
{

    public function __construct()
    {
        //
    }

    //Get All vendor 
    //TODO: still gotta apply pagination/cursor friendly
    //TODO: validation rule for alpha numeric with spaces and commas
    public function index()
    {
        $vendors = Vendor::all();

        $resource = new Collection($vendors, new \App\Transformers\Vendor());

        return (new Manager())->createData($resource)->toArray();
    }

    //Gets a single vendor
    public function show($vendorId)
    {
        $vendor = Vendor::where('id', $vendorId)->first();

        if(!$vendor){
            return response()->json([
                'data' => [
                    'errors' => 'vendor not found'
                ]
            ],204);
        }

        $resource = new Item($vendor, new \App\Transformers\Vendor());

        return (new Manager())->createData($resource)->toArray();
    }


    // Create vendor 
    public function create( Request $request )
    {
        $this->validate($request, [
          'name'         => 'required|alpha_num|unique:vendors',
          'established'  => 'required|numeric',
          'location'     => 'required|alpha'
        ]);

        $vendor                 = new Vendor();
        $vendor->id             = Uuid::uuid1();
        $vendor->name           = $request->get('name');
        $vendor->established    = $request->get('established');
        $vendor->location       = $request->get('location');
        $vendor->save();

        $resource = new Item($vendor, new \App\Transformers\Vendor());

        return (new Manager())->createData($resource)->toArray();
    }

    //update vendor
    public function update( Request $request, $vendorId )
    {
        $this->validate( $request, [
            'name'         => 'required|alpha_num|unique:vendors,name,'. $vendorId,
			'established'  => 'required|numeric',
            'location'     => 'required|alpha'
		] );

        $vendor = Vendor::where('id', $vendorId)->first();

        if(!$vendor){
            return response()->json([
                'data' => [
                    'error' => 'vendor not found'
                ]
            ],204);
        }

        $vendor->name           = $request->get('name');
        $vendor->established    = $request->get('established');
        $vendor->location       = $request->get('location');
        $vendor->save();

        $resource = new Item($vendor, new \App\Transformers\Vendor());

        return (new Manager())->createData($resource)->toArray();  
    }

    //delete vendor
    public function delete( $vendorId )
    {
        $vendor = Vendor::where('id', $vendorId)->first();

        if(!$vendor){
            return response()->json([
               'data' => [
                   'errors' => 'vendor not found'
               ] 
            ],204);
        }
		$vendor->delete();
        
        $resource = new Item($vendor, new \App\Transformers\Vendor());

        return (new Manager())->createData($resource)->toArray();  
    }
}
