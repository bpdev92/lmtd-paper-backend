<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Manager;
use App\Models\User;
use Ramsey\Uuid\Uuid;

class UsersController extends Controller
{

    //@TODO: rest of crud

    public function __construct()
    {
        //
    }

    //Get All users 
    //TODO: still gotta apply pagination/cursor friendly
    public function index()
    {
        $users = User::all();

        $resource = new Collection($users, new \App\Transformers\User());

        return (new Manager())->createData($resource)->toArray();
    }

    //Gets a single user
    public function show($userId)
    {
        $user = User::where('id', $userId)->first();

        if(!$user){
            return response()->json([
                'data' => [
                    'errors' => 'user not found'
                ]
            ],204);
        }

        $resource = new Item($user, new \App\Transformers\User());

        return (new Manager())->createData($resource)->toArray();
    }

    // Create user 
    public function create( Request $request )
    {
        $this->validate($request, [
          'first_name' => 'required|alpha_num',
          'last_name'  => 'required|alpha_num',
          'email'      => 'required|email|unique:users',
          'username'   => 'required|alpha_num|unique:users',
          'password'   => 'required|confirmed'
        ]);

        $user             = new User();
        $user->id         = Uuid::uuid1();
        $user->first_name = $request->get('first_name');
        $user->last_name  = $request->get('last_name');
        $user->email      = $request->get('email');
        $user->username   = $request->get('username');
        $user->password   = app('hash')->make($request->get('password'));
        $user->save();

        $resource = new Item($user, new \App\Transformers\User());

        return (new Manager())->createData($resource)->toArray();
    }

    //get user info for editing
    public function edit( $userId )
    {
        //probably dont need becasue its a SPA 
    }

    //update user
    public function update( Request $request, $userId )
    {
        $this->validate( $request, [
			'first_name' => 'required',
            'last_name'  => 'required',
            'username'   => 'required|unique:users,username,'. $userId,
			'email'      => 'required|email|unique:users,email,' . $userId,
			'password'   => 'sometimes|nullable|confirmed',
		] );

        $user = User::where('id', $userId)->first();

        if(!$user){
            return response()->json([
                'data' => [
                    'error' => 'user not found'
                ]
            ],204);
        }

        $user->first_name = $request->get('first_name');
        $user->last_name  = $request->get('last_name');
        $user->username   = $request->get('username');
        $user->email      = $request->get('email');
        if($request->get('password')){
            $user->password = app('hash')->make($request->get('password'));
        }
        $user->save();


        $resource = new Item($user, new \App\Transformers\User());

        return (new Manager())->createData($resource)->toArray();  
    }

    //delete user
    public function delete( $userId )
    {
        $user = User::where('id', $userId)->first();

        if(!$userId){
            return response()->json([
               'data' => [
                   'errors' => 'user not found'
               ] 
            ],204);
        }
		$user->delete();
        
        $resource = new Item($user, new \App\Transformers\User());

        return (new Manager())->createData($resource)->toArray();  
    }
}
