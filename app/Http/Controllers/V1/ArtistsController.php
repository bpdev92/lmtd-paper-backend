<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Manager;
use App\Models\Artist;
use Ramsey\Uuid\Uuid;

class ArtistsController extends Controller
{

    public function __construct()
    {
        //
    }

    //Get All artists 
    //TODO: still gotta apply pagination/cursor friendly
    //TODO: validation rule for alpha numeric with spaces and commas
    public function index()
    {
        $artists = Artist::orderBy('name')->get();
        $resource = new Collection($artists, new \App\Transformers\Artist());

        return (new Manager())->createData($resource)->toArray();
    }

    //Gets a single artist
    public function show($vendorId)
    {
        $vendor = Vendor::where('id', $vendorId)->first();

        if(!$vendor){
            return response()->json([
                'data' => [
                    'errors' => 'vendor not found'
                ]
            ],204);
        }

        $resource = new Item($vendor, new \App\Transformers\Vendor());

        return (new Manager())->createData($resource)->toArray();
    }


    // Create artist 
    public function create( Request $request )
    {
        $this->validate($request, [
          'first_name' => 'alpha',
          'last_name'  => 'alpha',
          'nickname'   => 'required'
        ]);

        $artist               = new Artist();
        $artist->id           = Uuid::uuid1();
        $artist->first_name   = $request->get('first_name');
        $artist->last_name    = $request->get('last_name');
        $artist->nickname     = $request->get('nickname');
        $artist->save();

        $resource = new Item($artist, new \App\Transformers\Artist());

        return (new Manager())->createData($resource)->toArray();
    }

    //update artist
    public function update( Request $request, $artistId )
    {
        $this->validate( $request, [
            'first_name'   => 'alpha',
			'last_name'    => 'alpha',
            'alias'        => 'alpha_num'
		] );

        $artist = Artist::where('id', $artistId)->first();

        if(!$artist){
            return response()->json([
                'data' => [
                    'error' => 'artist not found'
                ]
            ],204);
        }

        $artist->first_name   = $request->get('first_name');
        $artist->last_name    = $request->get('last_name');
        $artist->alias        = $request->get('alias');
        $artist->save();

        $resource = new Item($artist, new \App\Transformers\Artist());

        return (new Manager())->createData($resource)->toArray();  
    }

    //delete artist
    public function delete( $artistId )
    {
        $artist = Artist::where('id', $artistId)->first();

        if(!$artist){
            return response()->json([
               'data' => [
                   'errors' => 'artist not found'
               ] 
            ],204);
        }
		$artist->delete();
        
        $resource = new Item($artist, new \App\Transformers\Artist());

        return (new Manager())->createData($resource)->toArray();  
    }
}
