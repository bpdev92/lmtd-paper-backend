<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Rules\ValidEditionSize;
use Illuminate\Http\Request;
use League\Fractal\Pagination\Cursor;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Manager;
use App\Models\ArtPrint;
use App\Models\Artist;
use App\Models\Technique;
use App\Models\Manufacturer;
use App\Models\Vendor;
use App\Helpers\ArtPrintHelper;
use Ramsey\Uuid\Uuid;

class AddPrintController extends Controller
{
  public function getArtists()
  {
    $artists = Artist::orderBy('name')->get();
    $resource = new Collection($artists, new \App\Transformers\AddPrint\Artist());

    return (new Manager())->createData($resource)->toArray();
  }

  public function getVendors()
  {
    $vendors = Vendor::orderBy('name')->get();
    $resource = new Collection($vendors, new \App\Transformers\AddPrint\Vendor());

    return (new Manager())->createData($resource)->toArray();
  }

  public function getTechniques()
  {
    $techniques = Technique::orderBy('name')->get();
    $resource = new Collection($techniques, new \App\Transformers\AddPrint\Technique());

    return (new Manager())->createData($resource)->toArray();
  }

  public function getManufacturers()
  {
    $manufacturers = Manufacturer::orderBy('name')->get();
    $resource = new Collection($manufacturers, new \App\Transformers\AddPrint\Manufacturer());

    return (new Manager())->createData($resource)->toArray();
  }

  public function store(Request $request)
  {
    $this->validate($request, [
      'title'                 => 'required',
      'edition'               => 'required',//TODO: regex to see if the word edition is in there.
      'artist'                => 'required',
      'newArtist.name'        => 'required_if:artist,new',
      'vendor'                => 'required',
      'newVendor.name'        => 'required_if:vendor,new',
      'newVendor.location'    => 'required_if:vendor,new',
      'newVendor.year'        => 'required_if:vendor,new|numeric',
      'editionSize'           => ['required', new ValidEditionSize],
      'technique'             => 'required',
      'newTechnique.name'     => 'required_if:technique,new',
      'manufacturer'          => 'required',
      'newManufacturer.name'  => 'required_if:manufacturer,new',
      'originalPrice'         => 'required|numeric',
      'date'                  => 'required|date',
      'width'                 => 'required|numeric',
      'height'                => 'required|numeric',
      'status'                => 'required'
    ]);

    $title = ucwords(strtolower(trim($request->get('title'))));
    $edition = ucwords(strtolower(trim($request->get('edition'))));


    $print = ArtPrint::where('artist_id', $request->get('artist') )
                      ->where('vendor_id', $request->get('vendor'))
                      ->where('title', $title)
                      ->where('edition', $edition)
                      ->first();
   
    if($print) {
      return response()->json([
        'data' => [
          'errors' => 'Print has already been added.'
        ]
      ],422);
    }

    $artistId = $request->get('artist') === 'new' ? ArtPrintHelper::createArtist($request->get('newArtist')['name']) : $request->get('artist');
    $techniqueId = $request->get('technique') === 'new' ? ArtPrintHelper::createTechnique($request->get('newTechnique.name')) : $request->get('technique');
    $manufacturerId = $request->get('manufacturer') === 'new' ? ArtPrintHelper::createManufacturer($request->get('newManufacturer.name')) : $request->get('manufacturer');
    $vendorId = $request->get('vendor') === 'new' ? ArtPrintHelper::createVendor($request->get('newVendor')) : $request->get('vendor');

    $print = new ArtPrint();
    $print->id = Uuid::uuid4();
    $print->title = $title;
    $print->artist_id = $artistId;
    $print->picture = 'test';
    $print->edition = $edition;
    $print->edition_size = $request->get('editionSize');
    $print->technique_id = $techniqueId;
    $print->manufacturer_id = $manufacturerId;
    $print->vendor_id = $vendorId;
    $print->original_price = $request->get('originalPrice');
    $print->release_date = $request->get('date');
    $print->width = $request->get('width');
    $print->height = $request->get('height');
    $print->status = $request->get('status');
    $print->save();

    $resource = new Item($print, new \App\Transformers\ArtPrint());
    return (new Manager())->createData($resource)->toArray();


    // TODO: check if new artist, technique, manufacture, vendor is a duplicate 
    // TODO: success message for new print

  }
    
}
