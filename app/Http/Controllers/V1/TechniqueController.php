<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Manager;
use App\Models\Technique;
use Ramsey\Uuid\Uuid;

class TechniqueController extends Controller
{

    public function __construct()
    {
        //
    }

    public function index()
    {
      $techniques = Technique::orderBy('name')->get();
      $resource = new Collection($techniques, new \App\Transformers\Technique());

      return (new Manager())->createData($resource)->toArray();
    }

    //Gets a single artist
    public function show($techniqueId)
    {
      $technique = Technique::where('id', $techniqueId)->first();

      if(!$techniqueId){
          return response()->json([
              'data' => [
                  'errors' => 'technique not found'
              ]
          ],204);
      }

      $resource = new Item($technique, new \App\Transformers\Technique());

      return (new Manager())->createData($resource)->toArray();
    }


    // Create technique 
    public function create( Request $request )
    {
      $this->validate($request, [
        'name' => 'required',
      ]);

      $technique         = new Technique();
      $technique->id     = Uuid::uuid1();
      $technique->name   = $request->get('name');
      $technique->save();

      $resource = new Item($technique, new \App\Transformers\Technique());

      return (new Manager())->createData($resource)->toArray();
    }

    //update artist
    public function update( Request $request, $techniqueId )
    {
      $this->validate( $request, [
          'name'   => 'required',
      ] );

      $technique = Technique::where('id', $artistId)->first();

      if(!$technique){
          return response()->json([
              'data' => [
                  'error' => 'technique not found'
              ]
          ],204);
      }

      $technique->name   = $request->get('name');
      $technique->save();

      $resource = new Item($technique, new \App\Transformers\Technique());

      return (new Manager())->createData($resource)->toArray();  
    }

    //delete artist
    public function delete( $techniqueId )
    {
      $technique = Technique::where('id', $techniqueId)->first();

      if(!$technique){
          return response()->json([
              'data' => [
                  'errors' => 'technique not found'
              ] 
          ],204);
      }

      $technique->delete();
      
      $resource = new Item($technique, new \App\Transformers\Technique());

      return (new Manager())->createData($resource)->toArray();  
    }
}
