<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use League\Fractal\Pagination\Cursor;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Manager;
use App\Models\ArtPrint;
use Ramsey\Uuid\Uuid;

class ArtPrintController extends Controller
{
    
    const PAGE_LIMIT = 16;

    public function __construct()
    {
        //
    }

    public function index(Request $request) 
    {
        $page = $request->get('page');

        $offset = ($page * self::PAGE_LIMIT) - self::PAGE_LIMIT;

        $prints = ArtPrint::skip($offset)->orderBy('created_at', 'desc')->take(self::PAGE_LIMIT)->get();
        $total = ArtPrint::count();

        $resource = new Collection($prints, new \App\Transformers\ArtPrint());
        $pagination = [
            'pagination' => [
                'page' => $page,
                'limit' => self::PAGE_LIMIT,
                'count' => $prints->count(),
                'total' => $total,
                'totalPages' => floor($total/self::PAGE_LIMIT)
            ]
        ];

        $resource->setMeta($pagination);

        return (new Manager())->createData($resource)->toArray();
    }

    //Gets a single print
    public function show($printId)
    {
        $print = ArtPrint::where('id', $printId)->first();

        if(!$print){
            return response()->json([
                'data' => [
                    'errors' => 'print not found.'
                ]
            ],204);
        }

        $resource = new Item($print, new \App\Transformers\ArtPrint());

        return (new Manager())->createData($resource)->toArray();
    }

    // Create print 
    public function create( Request $request )
    {
        $this->validate($request, [
          'title'           => 'required|string',
          'artist_id'       => 'required',
          'vendor_id'       => 'required',
          'picture'         => 'required',
          'edition_size'    => 'required|numeric',
          'technique'       => 'string',
          'manufacture'     => 'required|string',
          'original_price'  => 'required|numeric',
          'release_date'    => 'required',
          'height'          => 'required|numeric',
          'width'           => 'required|numeric',
          'status'          => 'required|alpha'
        ]);

        $print                  = new ArtPrint();
        $print->id              = Uuid::uuid1();
        $print->title           = $request->get('title');
        $print->artist_id       = $request->get('artist_id');
        $print->vendor_id       = $request->get('vendor_id');
        //TODO: do something to upload an image to whereever i pay for
        $print->picture         = $request->get('picture');
        //
        $print->edition_size    = $request->get('edition_size');
        $print->technique       = $request->get('technique');
        $print->original_price  = $request->get('original_price');
        $print->release_date    = $request->get('release_date');
        $print->height          = $request->get('height');
        $print->width           = $request->get('width');
        $print->status          = $request->get('status');
        $print->save();

        $resource = new Item($print, new \App\Transformers\ArtPrint());

        return (new Manager())->createData($resource)->toArray();
    }

    //update print
    public function update( Request $request, $printId )
    {
        $this->validate($request, [
            'title'           => 'required|string',
            'artist_id'       => 'required',
            'vendor_id'       => 'required',
            'picture'         => 'required',
            'edition_size'    => 'required|numeric',
            'technique'       => 'string',
            'manufacture'     => 'required|string',
            'original_price'  => 'required|numeric',
            'release_date'    => 'required',
            'height'          => 'required|numeric',
            'width'           => 'required|numeric',
            'status'          => 'required|alpha'
        ]);

        $print = ArtPrint::where('id', $printId)->first();

        if(!$print){
            return response()->json([
                'data' => [
                    'error' => 'print not found.'
                ]
            ],204);
        }

        $print->title           = $request->get('title');
        $print->artist_id       = $request->get('artist_id');
        $print->vendor_id       = $request->get('vendor_id');
        //TODO: do something to upload an image to whereever i pay for
        $print->picture         = $request->get('picture');
        //
        $print->edition_size    = $request->get('edition_size');
        $print->technique       = $request->get('technique');
        $print->original_price  = $request->get('original_price');
        $print->release_date    = $request->get('release_date');
        $print->height          = $request->get('height');
        $print->width           = $request->get('width');
        $print->status          = $request->get('status');
        $print->save();

        $resource = new Item($print, new \App\Transformers\ArtPrint());

        return (new Manager())->createData($resource)->toArray();  
    }

    //delete print
    public function delete( $printId )
    {
        $print = ArtPrint::where('id', $printId)->first();

        if(!$print){
            return response()->json([
               'data' => [
                   'errors' => 'print not found.'
               ] 
            ],204);
        }
		$print->delete();
        
        $resource = new Item($print, new \App\Transformers\ArtPrint());

        return (new Manager())->createData($resource)->toArray();  
    }
}
