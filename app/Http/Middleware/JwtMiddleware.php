<?php

namespace App\Http\Middleware;
use Closure;
use Exception;
use App\Models\User;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

class JwtMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->header('X-Auth-Token');
        
        if(!$token) {
            return response()->json([
                'data' => [
                    'error' => 'Token not provided.'
                ]
            ], 401);
        }
        try {
            $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
        } catch(ExpiredException $e) {
            return response()->json([
                'data' => [
                    'error' => 'Provided token is expired.'
                ]
            ], 401);
        } catch(Exception $e) {
            return response()->json([
                'data' => [
                    'error' => 'An error while decoding token.'
                ]
            ], 401);
        }

        //check if its actually the user using that token ???

        $user = User::find($credentials->sub);

        $request->auth = $user;
        return $next($request);
    }
}