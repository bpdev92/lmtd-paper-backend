<?php

namespace App\Helpers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ArtPrint;
use App\Models\Artist;
use App\Models\Technique;
use App\Models\Manufacturer;
use App\Models\Vendor;
use Ramsey\Uuid\Uuid;

class ArtPrintHelper
{
  public static function createArtist($artistName)
  {
    $artist = new Artist();
    $artist->id = Uuid::uuid4();
    $artist->name = ucwords(strtolower(trim($artistName)));
    $artist->save();

    return $artist->id;
  }

  public static function createTechnique($techniqueName) 
  {
    $technique = new Technique();
    $technique->id = Uuid::uuid4();
    $technique->name = ucwords(strtolower(trim($techniqueName)));
    $technique->save();

    return $technique->id;
  }

  public static function createManufacturer($manufacturerName)
  {
    $manufacturer = new Manufacturer();
    $manufacturer->id = Uuid::uuid4();
    $manufacturer->name = ucwords(strtolower(trim($manufacturerName)));
    $manufacturer->save();

    return $manufacturer->id;
  }
  
  public static function createVendor($newVendor) 
  {
    $vendor = new Vendor();
    $vendor->id = Uuid::uuid4();
    $vendor->name = ucwords(strtolower(trim($newVendor['name'])));
    $vendor->established = $newVendor['year'];
    $vendor->location = $newVendor['location'];
    $vendor->save();

    return $vendor->id;
  }
    
}
