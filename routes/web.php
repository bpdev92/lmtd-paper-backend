<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


//@TODO: implement forgot password functionality

$router->group(['as' => 'v1', 'prefix' => 'v1', 'namespace' => 'V1'], function () use ($router) {

    $router->group([
            'as'     => 'auth',
            'prefix' => 'auth',
        ], function () use ($router) {

        $router->post('/authenticate', [
            'as'   => 'auth',
            'uses' => 'AuthController@Authenticate',
        ]);

        $router->post('/register', [
            'as'    => 'register',
            'uses'  => 'AuthController@register'
        ]);


    });

    //users crud
    $router->group([
            'as'     => 'users',
            'prefix' => 'users',
            'middleware' => 'jwt.auth'
        ], function () use ($router) {

        //get all users
        $router->get('/', [
            'as'    => 'users',
            'uses'  => 'UsersController@index'
        ]);

        //get user
        $router->get('/{userId}', [
            'as'    => 'user',
            'uses'  => 'UsersController@show'
        ]);

        //edit
        $router->get('/edit/{userId}', [
            'as'    => 'edit',
            'uses'  => 'UsersController@edit'
        ]);

        //update
        $router->patch('/{userId}', [
            'as'    => 'update',
            'uses'  => 'UsersController@update'
        ]);

        //delete
        $router->delete('/{userId}', [
            'as'    => 'delete',
            'uses'  => 'UsersController@delete'
        ]);


    });

    //vendor crud
    $router->group([
            'as'     => 'vendors',
            'prefix' => 'vendors',
            // 'middleware' => 'jwt.auth'
        ], function () use ($router) {

        //get all vendors
        $router->get('/', [
            'as'    => 'vendors',
            'uses'  => 'VendorsController@index'
        ]);

        //get vendor
        $router->get('/{vendorId}', [
            'as'    => 'user',
            'uses'  => 'VendorsController@show'
        ]);

        //create 
        $router->post('/create', [
            'as'    => 'create',
            'uses'  => 'VendorsController@create'
        ]);

        //update
        $router->patch('/{vendorId}', [
            'as'    => 'update',
            'uses'  => 'VendorsController@update'
        ]);

        //delete
        $router->delete('/{vendorId}', [
            'as'    => 'delete',
            'uses'  => 'VendorsController@delete'
        ]);


    });

    //artist crud
    $router->group([
            'as'     => 'artists',
            'prefix' => 'artists',
            // 'middleware' => 'jwt.auth'
        ], function () use ($router) {

        //get all artists
        $router->get('/', [
            'as'    => 'artists',
            'uses'  => 'ArtistsController@index'
        ]);

        //get artist
        $router->get('/{artistId}', [
            'as'    => 'artist',
            'uses'  => 'ArtistsController@show'
        ]);

        //create 
        $router->post('/create', [
            'as'    => 'create',
            'uses'  => 'ArtistsController@create'
        ]);

        //update
        $router->patch('/{artistId}', [
            'as'    => 'update',
            'uses'  => 'ArtistsController@update'
        ]);

        //delete
        $router->delete('/{artistId}', [
            'as'    => 'delete',
            'uses'  => 'ArtistsController@delete'
        ]);


    });

    //prints crud
    $router->group([
            'as'     => 'prints',
            'prefix' => 'prints',
            // 'middleware' => 'jwt.auth'
        ], function () use ($router) {

        //get all prints
        $router->get('/', [
            'as'    => 'prints',
            'uses'  => 'ArtPrintController@index'
        ]);

        $router->post('/', [
            'as'    => 'index',
            'uses'  => 'ArtPrintController@index'
        ]);

        //get print
        $router->get('/{artPrintId}', [
            'as'    => 'print',
            'uses'  => 'ArtPrintController@show'
        ]);

        //create print
        $router->post('/create', [
            'as'    => 'create',
            'uses'  => 'ArtPrintController@create'
        ]);

        // update print
        $router->patch('/{artPrintId}', [
            'as'    => 'update',
            'uses'  => 'ArtPrintController@update'
        ]);

        //delete print
        $router->delete('/{artPrintId}', [
            'as'    => 'delete',
            'uses'  => 'ArtPrintController@delete'
        ]);


    });

    //technique crud
    $router->group([
            'as'     => 'techniques',
            'prefix' => 'techniques',
        ], function () use ($router) {

        //get all prints
        $router->get('/', [
            'as'    => 'index',
            'uses'  => 'TechniqueController@index'
        ]);

        //get print
        $router->get('/{techniqueId}', [
            'as'    => 'technique',
            'uses'  => 'TechniqueController@show'
        ]);

        //create print
        $router->post('/create', [
            'as'    => 'create',
            'uses'  => 'TechniqueController@create'
        ]);

        // update print
        $router->patch('/{techniqueId}', [
            'as'    => 'update',
            'uses'  => 'TechniqueController@update'
        ]);

        //delete print
        $router->delete('/{techniqueId}', [
            'as'    => 'delete',
            'uses'  => 'TechniqueController@delete'
        ]);


    });

    // add print routes
    $router->group([
            'as'     => 'add-print',
            'prefix' => 'add-print',
        ], function () use ($router) {

        //get all techniques
        $router->post('/', [
            'as'    => 'store',
            'uses'  => 'AddPrintController@store'
        ]);

        //get all artists 
        $router->get('/artists', [
            'as'    => 'get-artists',
            'uses'  => 'AddPrintController@getArtists'
        ]);

        //get all vendors
        $router->get('/vendors', [
            'as'    => 'get-vendors',
            'uses'  => 'AddPrintController@getVendors'
        ]);

        //get all techniques
        $router->get('/techniques', [
            'as'    => 'get-techniques',
            'uses'  => 'AddPrintController@getTechniques'
        ]);

        //get all techniques
        $router->get('/manufacturers', [
            'as'    => 'get-manufacturers',
            'uses'  => 'AddPrintController@getManufacturers'
        ]);
    });
});